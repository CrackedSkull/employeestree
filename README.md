# EmployeesTree

## Getting started
Here you will see how to launch our application

## Use make to up/down/build
```
make up - build and run docker containers
make down - stop docker containers
make build - create images
make test - run test
```

## After start
To access the data, you need to click on the following link

[Employees hierarchy tree](http://0.0.0.0:8001/api/employees)
