import os
import pathlib
import typing

import aiohttp_jinja2
import jinja2
import yaml

from db import PostgresAccessor
from employees.routes import routes as employees_routes
from jinja2_custom_filters import extra_filters

BASE_DIR = pathlib.Path(__file__).parent
config_path = os.path.join(BASE_DIR, 'config/config.yaml')

DB_URL = 'postgresql+asyncpg://{db_username}:{db_secret}@{db_host}:{db_port}/{db_name}'


def get_config(path: str) -> typing.Dict:
    """
    Parses config file
    :param path: path to config file
    :return:
    """
    with open(path) as f:
        parsed_config = yaml.safe_load(f)
        return parsed_config


config = get_config(config_path)


def setup_config(application: 'Application') -> None:
    """
    Appends config to application
    :param application: Application instance
    """
    application['config'] = config


def setup_external_libraries(application: 'Application') -> None:
    """
    Sets external libraries
    :param application: Application instance
    """
    aiohttp_jinja2.setup(
        application,
        loader=jinja2.FileSystemLoader(f'{BASE_DIR}/templates')
    )
    extra_filters(application)


def setup_routes(application: 'Application') -> None:
    """
    Adds routes to applications
    :param application: Application instance
    """
    routes = [
        *employees_routes
    ]
    application.add_routes(routes)


def setup_db(application: 'Application') -> None:
    """
    Sets databases accessor
    :param application: Application instance
    """
    application['db'] = PostgresAccessor(DB_URL, application['config']['postgres'])


def setup_app(application: 'Application') -> None:
    """
    Sets application
    :param application: Application
    """
    setup_config(application)
    setup_external_libraries(application)
    setup_routes(application)
    setup_db(application)
