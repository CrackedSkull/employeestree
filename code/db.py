import typing
from contextlib import asynccontextmanager

from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import declarative_base, sessionmaker

Base = declarative_base()


class PostgresAccessor:
    """
    Postgres class for access to database
    """
    def __init__(self, db_url: str, db_settings: typing.Dict) -> None:
        """
        initialized class instance and create db engine
        :param db_url: database url with credentials
        :param db_settings: dict with params for database url
        """
        db_url = db_url.format(**db_settings)
        self.engine = create_async_engine(
            db_url,
            echo=True,
            future=True,
        )

    def async_session_generator(self) -> 'sessionmaker':
        """
        Generates session
        :return: sessionmaker instance
        """
        return sessionmaker(
            self.engine,
            class_=AsyncSession
        )

    @asynccontextmanager
    async def get_session(self):
        """
        Gets session
        :return: AsyncSession instance
        """
        try:
            async_session = self.async_session_generator()

            async with async_session() as session:
                yield session
        except:
            await session.rollback()
            raise
        finally:
            await session.close()
