from aiohttp import web

from settings import setup_app

app = web.Application()
setup_app(app)
web.run_app(app, host=app['config']['common']['host'], port=app['config']['common']['port'])
