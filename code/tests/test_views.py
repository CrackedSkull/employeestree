from html.parser import HTMLParser


class TagDataHTMLParser(HTMLParser):
    """
    Class for parse html tag
    """
    def __init__(self):
        super(TagDataHTMLParser, self).__init__()
        self.reset()
        self.tag_data = []
        self.lts_tag = None

    def handle_starttag(self, tag: str, attrs: str) -> None:
        """
        Gets latest start tag

        :param tag: current tag
        :param attrs: current tag params
        """
        self.lts_tag = tag

    def handle_data(self, data: str) -> None:
        """
        Checks data and last tag, then updates tag data

        :param data: str
        """
        if '\n' not in data and self.lts_tag == 'li':
            self.tag_data.append(data)


async def test_employees_view_get(aiohttp_client: 'function', app: 'aiohttp.web_app.Application'):
    """
    Tests employees view get method

    :param aiohttp_client: function that created client
    :param app: initialized application
    """
    client = await aiohttp_client(app)
    resp = await client.get('/api/employees')
    assert resp.status == 200
    text = await resp.text()
    html_parser = TagDataHTMLParser()
    html_parser.feed(text)
    assert {'Yoda', 'Mace Windu', 'Obi-Wan Kenobi', 'Luke Skywalker', 'Darth Sidious', 'Darth Vader', 'Galen Marek',
            'Darth Maul'} == set(html_parser.tag_data)
