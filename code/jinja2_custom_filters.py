import typing

import aiohttp_jinja2


def extra_filters(app: 'Application') -> typing.Dict:
    """
    Sets extra jinja filters
    :param app: Application instance
    :return: dict with filter name as a key and filter function as a value
    """
    filters = aiohttp_jinja2.get_env(app).filters
    filters['hierarchy'] = lambda obj: obj.as_html()
    return filters
