from sqlalchemy import VARCHAR, Column, DateTime, ForeignKey, Integer, func
from sqlalchemy.orm import relationship

from db import Base


class Employees(Base):
    __tablename__ = "employees"

    employee_id = Column('employee_id', Integer, primary_key=True)
    employee_firstname = Column('employee_firstname', VARCHAR(255), nullable=False)
    employee_lastname = Column('employee_lastname', VARCHAR(255), nullable=True)
    employee_boss_id = Column('employee_boss_id', Integer, ForeignKey(
        'employees.employee_id', onupdate='CASCADE', ondelete='SET NULL'
    ), nullable=True)
    modified_date = Column(
        'modified_date', DateTime, nullable=False, server_default=func.now(), onupdate=func.current_timestamp()
    )
    created_date = Column('created_date', DateTime, nullable=False, server_default=func.now())
    employee_boss = relationship('Employees', remote_side=[employee_id])
