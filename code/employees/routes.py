from aiohttp import web

from .views import EmployeesView

routes = [web.view('/api/employees', EmployeesView)]
