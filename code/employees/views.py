import asyncio
from concurrent.futures import ProcessPoolExecutor
import typing

import aiohttp_jinja2
from aiohttp import web
from sqlalchemy.future import select
from sqlalchemy.orm import aliased

from .models import Employees


class EmployeeHierarchyNode(dict):
    """
    Hierarchy node class
    """

    def __init__(self, full_name: str) -> None:
        """
        initialized employee hierarchy node instance
        :param full_name: employee full name
        """
        super(EmployeeHierarchyNode, self).__init__()
        self._parent = None
        self['full_name'] = full_name
        self['children'] = []

    @property
    def parent(self) -> 'EmployeeHierarchyNode':
        """
        Gets parent pointer
        :return: employee hierarchy node instance
        """
        return self._parent

    @parent.setter
    def parent(self, node: 'EmployeeHierarchyNode') -> None:
        """
        Sets parent pointer
        :param node: employee hierarchy node instance
        """
        self._parent = node
        node['children'].append(self)

    def as_html(self, level: int = 0) -> str:
        """
        Creates hierarchy hrml list
        :param level:
        :return: html element as a string
        """
        ret = "<li>" + self['full_name'] + "</li>"
        if self['children']:
            ret += "<ul>"
            for child in self['children']:
                ret += child.as_html(level + 1)
            ret += "</ul>"
        return ret


class EmployeesView(web.View):
    """
    Employees view instance
    """

    @staticmethod
    def build_hierarchy(boss_employees_list: typing.List[typing.Tuple]) -> typing.List:
        """
        Creates hierarchy
        :param boss_employees_list:
        :return: list of hierarchies
        """
        lookup = {}
        as_a_children = set()
        for pUID, uid in boss_employees_list:
            this = lookup.get(uid)
            as_a_children.add(uid)
            if this is None:
                this = EmployeeHierarchyNode(uid)
                lookup[uid] = this
            parent = lookup.get(pUID)
            if not parent:
                parent = EmployeeHierarchyNode(pUID)
                lookup[pUID] = parent
            this.parent = parent
        return [lookup[key] for key in set(lookup.keys()) - as_a_children]

    @aiohttp_jinja2.template('hierarchy.html')
    async def get(self) -> typing.Dict:
        """
        Gets all users
        :return: context dict for jinja template
        """
        async with self.request.app['db'].get_session() as session:
            async with session.begin():
                Boss = aliased(Employees, name='boss')
                stmt = select(
                    Boss.employee_firstname,
                    Boss.employee_lastname,
                    Employees.employee_firstname,
                    Employees.employee_lastname
                ).join(
                    Boss, Employees.employee_boss_id == Boss.employee_id
                ).order_by(
                    Employees.employee_boss_id
                )
                boss_employee = await session.execute(stmt)
                bosses_and_employees = list(
                    (' '.join(pair[:2] if pair[1] else pair[0:1]), ' '.join(pair[2:] if pair[3:] else pair[2]))
                    for pair in boss_employee.all()
                )
            await session.commit()
        loop = asyncio.get_event_loop()
        build_hierarchy_task = loop.run_in_executor(ProcessPoolExecutor(max_workers=3), self.build_hierarchy,
                                                    bosses_and_employees)
        hierarchy = await build_hierarchy_task
        return {'data': hierarchy}
