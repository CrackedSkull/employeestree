import os
from contextlib import asynccontextmanager

import pytest
from aiohttp import web
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

import alembic.command
from alembic.config import Config
from employees.views import EmployeesView
from settings import setup_external_libraries

ROUTES = [web.view('/api/employees', EmployeesView)]


class SQLLiteAccessor:
    """
    SQL lite class for access to database
    """

    def __init__(self) -> None:
        """
        initialized class instance and create db engine
        """
        self.engine = create_async_engine(
            'sqlite+aiosqlite:///database.db',
            echo=True,
            future=True,
        )

    def async_session_generator(self) -> 'sessionmaker':
        """
        Generates session
        :return: sessionmaker instance
        """
        return sessionmaker(
            self.engine,
            class_=AsyncSession
        )

    @asynccontextmanager
    async def get_session(self) -> 'AsyncSession':
        """
        Gets session
        :return: AsyncSession instance
        """
        try:
            async_session = self.async_session_generator()

            async with async_session() as session:
                yield session
        except:
            await session.rollback()
            raise
        finally:
            os.remove(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'database.db'))
            await session.close()


@pytest.fixture(autouse=True)
def migrate() -> None:
    """
    Applies test migrations
    """
    __config_path__ = os.path.dirname(os.path.abspath(__file__))
    __migration_path__ = os.path.join(os.path.dirname(os.path.abspath(__file__)), "alembic")

    alembic_cfg = Config(__config_path__)
    alembic_cfg.set_main_option("script_location", __migration_path__)
    alembic_cfg.set_main_option('sqlalchemy.url', 'sqlite+aiosqlite:///database.db')
    alembic_cfg.set_main_option('version_locations', 'tests/migrations')
    alembic.command.upgrade(alembic_cfg, "head")


@pytest.fixture
def app() -> 'Application':
    """
    Sets application instance
    :return: Application instance
    """
    app = web.Application()
    setup_external_libraries(app)
    app.router.add_routes(ROUTES)
    app['db'] = SQLLiteAccessor()
    return app
