.PHONY: build up down
build:
	docker-compose --env-file .env.dev -f docker-compose.yml build
up:
	docker-compose --env-file .env.dev -f docker-compose.yml up -d
down:
	docker-compose --env-file .env.dev -f docker-compose.yml down
test:
	cd ./code && python -m pytest .